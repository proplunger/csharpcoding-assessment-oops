﻿using System;

class Developer
{
    public int Id { get; set; }
    public string DeveloperName { get; set; }
    public DateTime JoingDate { get; set; }
    public string Project_Assigned { get; set; }
    public bool OnContract { get; set; }
    public int Duration { get; set; }
    public decimal Charges_Per_Day { get; set; }
    public bool OnPayroll { get; set; }
    public string Dept { get; set; }
    public string Manager { get; set; }
    public decimal NetSalary { get; set; }
    public int Exp { get; set; }

    // Constructor for OnContract Developers
    public Developer(int id, string name, DateTime joiningDate, string project, bool onContract, int duration, decimal chargesPerDay)
    {
        Id = id;
        DeveloperName = name;
        JoingDate = joiningDate;
        Project_Assigned = project;
        OnContract = onContract;
        Duration = duration;
        Charges_Per_Day = chargesPerDay;
        OnPayroll = false;
        Dept = "";
        Manager = "";
        NetSalary = 0;
        Exp = 0;
    }

    // Constructor for OnPayroll Developers
    public Developer(int id, string name, DateTime joiningDate, string project, bool onPayroll, string dept, string manager, decimal netSalary, int exp)
    {
        Id = id;
        DeveloperName = name;
        JoingDate = joiningDate;
        Project_Assigned = project;
        OnContract = false;
        Duration = 0;
        Charges_Per_Day = 0;
        OnPayroll = onPayroll;
        Dept = dept;
        Manager = manager;
        NetSalary = netSalary;
        Exp = exp;
    }

    // Method to calculate the total charges for OnContract Developers
    public decimal CalculateTotalCharges()
    {
        return Duration * Charges_Per_Day;
    }

    // Method to calculate the net salary for OnPayroll Developers
    public decimal CalculateNetSalary()
    {
        decimal da = NetSalary * 0.2m;
        decimal hra = NetSalary * 0.3m;
        decimal lta = NetSalary * 0.1m;
        decimal pf = NetSalary * 0.05m;

        return NetSalary + da + hra + lta - pf;
    }
}
