﻿using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static List<Developer> developers = new List<Developer>();

    static void Main(string[] args)
    {
        int option = 0;

        do
        {
            Console.WriteLine("\n==== Developer Management System ====");
            Console.WriteLine("1. Create Developer");
            Console.WriteLine("2. Calculate Total Charges");
            Console.WriteLine("3. Calculate Net Salary");
            Console.WriteLine("4. Display All Developers");
            Console.WriteLine("5. Display Developers with Net Salary > 20000");
            Console.WriteLine("6. Display Developers whose name contains 'D'");
            Console.WriteLine("7. Exit");

            try
            {
                Console.Write("\nEnter your option: ");
                option = int.Parse(Console.ReadLine());

                switch (option)
                {
                    case 1:
                        CreateDeveloper();
                        break;
                    case 2:
                        CalculateTotalCharges();
                        break;
                    case 3:
                        CalculateNetSalary();
                        break;


                    case 4:
                        DisplayDevelopers(developers);
                        break;
                    case 5:
                        var highSalaryDevelopers = developers.Where(d => d.OnPayroll && d.NetSalary > 20000);
                        DisplayDevelopers(highSalaryDevelopers.ToList());
                        break;
                    case 6:
                        var nameContainsD = developers.Where(d => d.DeveloperName.Contains("D"));
                        DisplayDevelopers(nameContainsD.ToList());
                        break;
                    case 7:
                        Console.WriteLine("Thank you for using the Developer Management System!");
                        break;
                    default:
                        Console.WriteLine("Invalid option, please try again.");
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        } while (option != 7);
    }

    static void CreateDeveloper()
    {
        Console.WriteLine("\nCreate Developer:");

        Console.Write("Enter Id: ");
        int id = int.Parse(Console.ReadLine());

        Console.Write("Enter Developer Name: ");
        string name = Console.ReadLine();

        Console.Write("Enter Joining Date (dd/MM/yyyy): ");
        DateTime joiningDate = DateTime.Parse(Console.ReadLine());

        Console.Write("Enter Project Assigned: ");
        string project = Console.ReadLine();

        Console.Write("Is the Developer On Contract? (true/false): ");
        bool onContract = bool.Parse(Console.ReadLine());

        if (onContract)
        {
            Console.Write("Enter Duration (in days): ");
            int duration = int.Parse(Console.ReadLine());

            Console.Write("Enter Charges Per Day: ");
            decimal chargesPerDay = decimal.Parse(Console.ReadLine());

            developers.Add(new Developer(id, name, joiningDate, project, true, duration, chargesPerDay));

            Console.WriteLine("On Contract Developer created successfully.");
        }
        else
        {
            Console.Write("Enter Department: ");
            string dept = Console.ReadLine();

            Console.Write("Enter Manager: ");
            string manager = Console.ReadLine();

            Console.Write("Enter Net Salary: ");
            decimal netSalary = decimal.Parse(Console.ReadLine());

            Console.Write("Enter Experience (in years): ");
            int exp = int.Parse(Console.ReadLine());

            developers.Add(new Developer(id, name, joiningDate, project, false, dept, manager, netSalary, exp));

            Console.WriteLine("On Payroll Developer created successfully.");
        }
    }

    static void CalculateTotalCharges()
    {
        Console.WriteLine("\nCalculate Total Charges:");

        Console.Write("Enter Developer Id: ");
        int id = int.Parse(Console.ReadLine());

        Developer developer = developers.FirstOrDefault(d => d.Id == id && d.OnContract);

        if (developer == null)
        {
            Console.WriteLine("On Contract Developer with the given Id not found.");
            return;
        }

        decimal totalCharges = developer.CalculateTotalCharges();

        Console.WriteLine($"Total Charges for Developer {developer.DeveloperName} is {totalCharges}.");
    }

    static void CalculateNetSalary()
    {
        Console.WriteLine("\nCalculate Net Salary:");

        Console.Write("Enter Developer Id: ");
        int id = int.Parse(Console.ReadLine());

        Developer developer = developers.FirstOrDefault(d => d.Id == id && d.OnPayroll);

        if (developer == null)
        {
            Console.WriteLine("On Payroll Developer with the given Id not found.");
            return;
        }

        decimal netSalary = developer.CalculateNetSalary();

        Console.WriteLine($"Net Salary for Developer {developer.DeveloperName} is {netSalary}.");
    }

    static void DisplayDevelopers(List<Developer> developers)
    {
        Console.WriteLine("\nDevelopers List:");
        Console.WriteLine("{0,-5} {1,-20} {2,-15} {3,-20} {4,-10} {5,-10} {6,-10} {7,-15} {8,-10} {9,-10} {10,-10}",
            "Id", "Name", "Joining Date", "Project Assigned", "On Contract", "Duration", "Charges/Day", "On Payroll", "Dept", "Manager", "Net Salary");

        foreach (Developer developer in developers)
        {
            if (developer.OnContract)
            {
                Console.WriteLine("{0,-5} {1,-20} {2,-15:d} {3,-20} {4,-10} {5,-10} {6,-10} {7,-15} {8,-10} {9,-10} {10,-10}",
                    developer.Id, developer.DeveloperName, developer.JoiningDate, developer.Project_Assigned,
                    "Yes", developer.Duration, developer.Charges_Per_Day, "No", "-", "-", "-");
            }
            else
            {
                Console.WriteLine("{0,-5} {1,-20} {2,-15:d} {3,-20} {4,-10} {5,-10} {6,-10} {7,-15} {8,-10} {9,-10} {10,-10}",
                    developer.Id, developer.DeveloperName, developer.JoiningDate, developer.Project_Assigned,
                    "No", "-", "-", "Yes", developer.Dept, developer.Manager, developer.NetSalary);
            }
        }
    }
}

